package com.aswinwibisurya.booksencyclopedia;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class BooksAdapter extends RecyclerView.Adapter<BooksAdapter.ViewHolder> {

    ArrayList<Book> listBooks;
    Context ctx;

    public BooksAdapter(ArrayList<Book> listBooks, Context ctx) {
        this.listBooks = listBooks;
        this.ctx = ctx;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(ctx);
        View itemView = inflater.inflate(R.layout.item_row_book, viewGroup, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        Book book = listBooks.get(position);
        viewHolder.ivThumbnail.setImageResource(book.thumbnail);
        viewHolder.tvTitle.setText(book.title);
    }

    @Override
    public int getItemCount() {
        return listBooks.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView ivThumbnail;
        TextView tvTitle;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ivThumbnail = itemView.findViewById(R.id.ivThumbnail);
            tvTitle = itemView.findViewById(R.id.tvTitle);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    Book book = listBooks.get(position);
                    String message = String.format("Title: %s, Author: %s, Price: %f", book.title, book.author, book.price);
                    Toast.makeText(ctx, message, Toast.LENGTH_LONG).show();
                }
            });
        }
    }
}
