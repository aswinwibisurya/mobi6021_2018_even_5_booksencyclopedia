package com.aswinwibisurya.booksencyclopedia;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ArrayList<Book> listBooks;
    RecyclerView rvBooks;
    BooksAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listBooks = new ArrayList<>();

        Book book1 = new Book();
        book1.title = "Beginning Android Application Development";
        book1.author = "Wei-Meng Lee";
        book1.price = 50.5;
        book1.thumbnail = R.drawable.image1;

        Book book2 = new Book();
        book2.title = "Harry Potter";
        book2.author = "J.K. Rowling";
        book2.price = 60.9;
        book2.thumbnail = R.drawable. image2;

        listBooks.add(book1);
        listBooks.add(book2);

        rvBooks = findViewById(R.id.rvBooks);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rvBooks.setLayoutManager(linearLayoutManager);

        adapter = new BooksAdapter(listBooks, this);
        rvBooks.setAdapter(adapter);
    }
}
