package com.aswinwibisurya.booksencyclopedia;

public class Book {
    public String title;
    public String author;
    public double price;
    public int thumbnail;
}
